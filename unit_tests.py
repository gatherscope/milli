from datetime import datetime
import time
import unittest

from feed_crawler import classify_language, query_feeds_data


class CrawlerMethods(unittest.TestCase):
    def setUp(self):
        self.startTime = time.time()

    def tearDown(self):
        t = time.time() - self.startTime
        print("%s: %.3f" % (self.id(), t))

    def test_classify_language(self):
        txt = 'The quick brown fox jumps over the fence.'
        lang = classify_language(txt)
        self.assertEqual(lang, 'en')

        txt = 'Me llamo es Roberto.'
        lang = classify_language(txt)
        self.assertEqual(lang, 'es')

    def test_query_feeds_data(self):
        feeds = query_feeds_data()
        feed = feeds[0]
        n = len(feeds)

        self.assertTrue(isinstance(feed[0], str))
        self.assertTrue(isinstance(feed[1], datetime))
        self.assertTrue(isinstance(feed[2], str))
        self.assertGreater(n, 10)

    def test_parse_articles_from_feed(self):
        # TODO: add actual unit tests for crawling
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
