from datetime import datetime, timedelta, timezone

from feedparser import parse
import langdetect
import newspaper
import psycopg2
from pymongo import MongoClient
from redis import Redis
from rq import Queue

import configs

# MongoDB Config
MDB_CLIENT = MongoClient(configs.MDB_HOST)
MILLI_MDB = MDB_CLIENT.milli_mdb
PARSED_CONTENT = MILLI_MDB.parsed_content

# Redis Config
REDIS_CONN = Redis()
REDIS_QUEUE = Queue(connection=REDIS_CONN)


def enqueue_feeds_for_crawling():
    """Queries for news feeds to be crawled.

    Queries Milli's PostgreSQL database for news feeds (rss and atom) to be
    crawled.

    Keyword arguments:
    None

    Returns:
    None, but enqueues into rq a parse_articles_from_feed job
    """

    try:
        feeds_data = query_feeds_data()
        for row in feeds_data:
            feed_url = row[0]
            feed_type = row[2]

            current_timestamp = datetime.utcnow().replace(tzinfo=timezone.utc)

            REDIS_QUEUE.enqueue(
                parse_articles_from_feed,
                feed_url,
                feed_type
            )
            update_last_queued_timestamp(feed_url, current_timestamp)
    except psycopg2.OperationalError:
        print('Unable to connect to PostgreSQL database!')


def query_feeds_data():
    """Queries PostgreSQL for rss and atom feeds.

    Keyword arguments:
    None

    Returns:
    list -- url, last_queued_timestamp, feed_type of feeds in PostgreSQL
    """

    sql = """
        SELECT url, last_queued_timestamp, feed_type, publisher
        FROM feeds
        WHERE crawl_priority >= 0
        ORDER BY publisher, crawl_priority
    """
    # ^ignore feeds that have been turned off

    pg_conn = psycopg2.connect(host=configs.PG_HOST,
                               dbname=configs.PG_DBNAME,
                               user=configs.PG_USER,
                               password=configs.PG_PASSWORD)
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute(sql)
    feeds_data = pg_cursor.fetchall()
    pg_cursor.close()
    pg_conn.close()

    return feeds_data


def parse_articles_from_feed(feed_url, feed_type):
    """Parse a feed url for articles.

    Given a feed url, first parses the feed for articles and other data.
    Then each article url found in the feed is passed to
    parse_and_save_article.

    Keyword arguments:
    feed_url -- rss or atom url to be parsed (string)
    feed_type -- whether the feed_url should be crawled with 'newspaper' or
        'feedparser' (string)

    Returns:
    None, but calls parse_article_and_save_results if inputs valid
    """

    if feed_type == 'feedparser':
        print('\n ***** CRAWLING FEED WITH FEEDPARSER: ******')
        print(feed_url)
        parsed_output = parse(feed_url,
                              agent='gatherscope/1.0 +http://gatherscope.com/',
                              referrer='http://gatherscope.com')

        print('***** PARSING ARTICLES: ******')
        if parsed_output['entries']:
            for entry in parsed_output['entries']:
                try:
                    article_url = entry['links'][0]['href']
                    article_url = article_url.split('?')[0]
                    article_url = article_url.split('#')[0]
                    print(article_url)
                    if not skip_article_url(article_url):
                        parse_article_and_save_results(article_url, feed_url)
                except:
                    print('Parse error :(')
        else:
            print('No articles to parse :(')
    elif feed_type == 'newspaper':
        print('\n ***** CRAWLING FEED WITH NEWSPAPER: ******')
        print(feed_url)
        parsed_output = newspaper.build(feed_url,
                                        memoize_articles=False)

        print('***** PARSING ARTICLES: ******')
        if parsed_output.articles:
            for entry in parsed_output.articles:
                try:
                    article_url = entry.url
                    article_url = article_url.split('?')[0]
                    article_url = article_url.split('#')[0]
                    # TODO: creating another article object is redundant
                    print(article_url)
                    if not skip_article_url(article_url):
                        parse_article_and_save_results(article_url, feed_url)
                except:
                    print('Parse error :(')
        else:
            print('No articles to parse :(')
    else:
        print('Invalid feed type :(')


def skip_article_url(article_url):
    skip = False
    for str in configs.ARTICLE_URL_BLACKLIST:
        skip = (skip or (str in article_url))

    return skip


def parse_article_and_save_results(article_url, feed_url=None):
    """Create a newspaper Article object and write to Mongo.

    Given an article url, checks to see if that article exists in MongoDB.
    If not, creates a newspaper Article object with the url and writes key data
    to MongoDB.

    Keyword arguments:
    article_url -- article url to be parsed (string)
    feed_url -- feed url that an article belongs to (string, default: None)

    Returns:
    None, but saves article data and metadata to MongoDB
    """

    if PARSED_CONTENT.count_documents({'url': article_url}) == 0:
        feed_url_2_pub = {url: pub for url, lqt, ft, pub in query_feeds_data()}
        np_article = newspaper.Article(article_url)
        np_article.download()
        np_article.parse()
        content_data = {
            'url': article_url,
            'feed_url': feed_url,
            'publisher': (feed_url_2_pub[feed_url]
                          if feed_url in feed_url_2_pub
                          else None),
            'crawled_timestamp': str(datetime.utcnow()),
            'title': np_article.title,
            'authors': np_article.authors,
            'published_date': np_article.publish_date,
            'text': np_article.text,
            'top_image': np_article.top_image,
            'meta_keywords': np_article.meta_keywords,
            'title_language': classify_language(np_article.title),
        }
        PARSED_CONTENT.insert_one(content_data)
        print('Parse complete :)')
    else:
        print('Already parsed :)')


def update_last_queued_timestamp(feed_url, updated_timestamp):
    """Update a feed's last_queued_timestamp in PostgreSQL.

    Given a feed url and a timestamp, updates the last_queued_timestamp value
    in PostgreSQL corresponding to the feed url in postgres to the timestamp.

    Keyword arguments:
    feed_url -- feed whose last queued timestamp should be updated
    updated_timestamp -- timestamp to update it to (datetime)

    Returns:
    None, but sets the last_queued_timestamp of a feed to updated_timestamp in
        PostgreSQL
    """

    sql = """
        UPDATE feeds
        SET last_queued_timestamp = %s
        WHERE url = %s
    """
    pg_conn = psycopg2.connect(host=configs.PG_HOST,
                               dbname=configs.PG_DBNAME,
                               user=configs.PG_USER,
                               password=configs.PG_PASSWORD)
    pg_cursor = pg_conn.cursor()
    pg_cursor.execute(sql, (updated_timestamp, feed_url))
    pg_conn.commit()
    pg_cursor.close()
    pg_conn.close()


def classify_language(txt):
    try:
        language = langdetect.detect(txt)
        return language
    except:
        return 'unknown'
