import json

import boto3


# AWS secret manager
AWS_REGION_NAME = "us-east-2"


def get_secret(secret_name):
    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=AWS_REGION_NAME
    )

    get_secret_value_response = client.get_secret_value(
        SecretId=secret_name
    )

    secret = get_secret_value_response['SecretString']

    return secret


# PostgreSQL Connection
PG_HOST = 'gatherscope-us.crdrxdtqjqpf.us-east-2.rds.amazonaws.com'
PG_DBNAME = 'gatherscope_milli'
PG_CREDS = json.loads(get_secret('postgresql/milli/creds'))
PG_USER = PG_CREDS['username']
PG_PASSWORD = PG_CREDS['password']


# MongoDB Connection
MDB_CREDS = json.loads(get_secret('mongodb/milli/creds'))
MDB_USERNAME = MDB_CREDS['username']
MDB_PASSWORD = MDB_CREDS['password']
MDB_HOST = (("mongodb+srv://%s:%s@gatherscope-us-gg0fy"
             + ".mongodb.net/test?retryWrites=true")
            % (MDB_USERNAME, MDB_PASSWORD))


ARTICLE_URL_BLACKLIST = ['.pdf']
